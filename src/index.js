import * as Clipboard from 'expo-clipboard';
import { Dimensions,Platform  } from 'react-native'; 
import * as DeviceInfo from 'expo-device';
import DankexCore from "./dankex-rn-core"; 
let App = {     
    getInstallQuery(op) {
     
        DankexCore.getInstallQuery(op,{
            Clipboard,Dimensions ,DeviceInfo,Platform       
        });
    }, 
}
export default App;