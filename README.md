# react-native-dankex

# 介绍
蛋壳追踪 react native SDK

# 插件简介

本插件用于React Native 开发的安卓/IOS APP, 

- APP携带参数安装 
- 免填写邀请码,绑定上下级
- 渠道统计
- 一键拉起
 
蛋壳追踪同时支持多平台开发SDK( uniapp, 原生安卓,IOS,flutter, react-native)
 
 
# Demo

[演示demo >>](https://www.dankex.cn/user/#/pages/demo/index?code=12345678&ref=uniapp-jssdk)

该demo演示了, 从安装落地页(含有自定义的安装参数), 然后下载 安卓/IOS APP;

启动app后可以获取自定义的安装参数;

# 5分钟接入流程

- 1.蛋壳追踪官网注册,创建应用并配置,获取appkey , [点击这里注册 >>](https://www.dankex.cn/)
- 2.APP下载落地页 部署websdk ,  [websdk部署文档点击这里 >> ](https://www.dankex.cn/docs/websdk.html)
- 3.ReactNative项目导入此插件 
 

## 安装

```
npm i react-native-dankex
or
yarn add react-native-dankex
expo
yarn expo install react-native-dankex
```


## 使用

```
import DankeX from 'react-native-dankex';
```

```
DankeX.getInstallQuery({
    appkey: 'your appkey',  
    success(query){
        console.log('query=',query);  //query="code=123456&from=wx"  , 如果无参,query=""
    }

})
``` 
appkey: 登录蛋壳官网 https://www.dankex.cn 获取

例如你的落地页地址是: https://xxxxxxx.com/?code=12345678&ref=uniapp-jssdk

那么你获取的query参数为: code=12345678&ref=uniapp-jssdk

# 接入问题

![微信在线](http://res.dankex.cn/sdk/kefu-qy.jpg?v2)

